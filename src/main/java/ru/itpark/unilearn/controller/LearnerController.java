package ru.itpark.unilearn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itpark.unilearn.entity.Language;
import ru.itpark.unilearn.entity.Tutorial;
import ru.itpark.unilearn.entity.Learner;
import ru.itpark.unilearn.entity.Status;
import ru.itpark.unilearn.service.LearnerService;

import java.util.List;

@RestController
@RequestMapping("/learners")
public class LearnerController {
    private final LearnerService service;

    @Autowired
    public LearnerController(LearnerService service) {
        this.service = service;
    }

    @GetMapping
    public List<Learner> getAll(){return  service.findAll();}

    @PostMapping
    public void add(@RequestParam String firstName, @RequestParam String secondName){
        Learner learner = new Learner(firstName, secondName);
        service.add(learner);
    }

    @PostMapping(path = "/{id}/purchase")
    public void addPurchase(@RequestParam int learnerId, @RequestParam int tutorId){
        service.addPurchase(learnerId, tutorId);
    }

    @GetMapping("/{id}/tutorials")
    public List<String> getTutorialsList(@RequestParam int learnerId){
        return service.getTutorsList(learnerId);
    }

    @DeleteMapping
    public void remove(@RequestParam int id){service.remove(id);}

    @GetMapping(path = "/searchByFirstName", params = "firstName")
    public List<Learner> searchByFirstName(@RequestParam String firstName){
        return service.findByFirstName(firstName);
    }

    @GetMapping(path = "/searchByStatus", params = "status")
    public List<Learner> searchByStatus(@RequestParam Status status){
        return service.findByStatus(status);
    }

    @PostMapping("/{id}")
    public void update(@RequestParam int learnerId){
        service.update(service.getById(learnerId));
    }

    @PostMapping("/{id}/challenge")
    public String makeChallenge(@RequestParam int initId, @RequestParam int targetId, @RequestParam Language language){
        return service.makeChallenge(initId, targetId, language);
    }
}