package ru.itpark.unilearn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itpark.unilearn.entity.Tutorial;
import ru.itpark.unilearn.service.TutorialService;

import java.util.List;

@RestController
@RequestMapping("/tutors")
public class TutorialController {
    private final TutorialService service;

    @Autowired
    public TutorialController(TutorialService service) {
        this.service = service;
    }

    @GetMapping
    public List<Tutorial> getAll() {
        return service.findAll();
    }

    @PostMapping
    public void add(@RequestBody Tutorial tutorial){
        service.add(tutorial);
    }
}
