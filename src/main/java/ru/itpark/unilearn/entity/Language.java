package ru.itpark.unilearn.entity;

public enum Language {
    ENGLISH, SPANISH, CHINESE, RUSSIAN, ARAB, FRENCH, PORTUGUESE, JAPANESE, TURKISH, GERMAN
}
