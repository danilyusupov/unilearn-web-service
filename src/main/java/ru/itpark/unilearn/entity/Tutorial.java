package ru.itpark.unilearn.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tutorials")
public class Tutorial implements Serializable {
    private static final long serialVersionUID = -3925433755108025146L;
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private Language language;
    private int prise;
    private int experience;

    public Tutorial() {
    }

    public Tutorial(String name, Language language, int prise, int experience) {
        this.name = name;
        this.language = language;
        this.prise = prise;
        this.experience = experience;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public int getPrise() {
        return prise;
    }

    public void setPrise(int prise) {
        this.prise = prise;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    @Override
    public String toString() {
        return "Tutorial{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", language=" + language +
                ", prise=" + prise +
                ", experience=" + experience +
                '}';
    }
}
