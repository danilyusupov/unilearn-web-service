package ru.itpark.unilearn.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Learner {
    @Id
    @GeneratedValue
    private int id;
    private String firstName;
    private String secondName;
    private int experience;
    private int level;
    //Counting money in cents!
    private int moneySpent;
    private Status status = Status.DEFAULT;
    @ElementCollection
    @CollectionTable(name = "tutorials_fields")
    @Column(name = "tutorial")
    private List<String> tutorsList = new ArrayList<>();
    private int challengeVictories;
    private int challengeDefeats;

    public Learner() {
    }

    public Learner(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getMoneySpent() {
        return moneySpent;
    }

    public void setMoneySpent(int moneySpent) {
        this.moneySpent = moneySpent;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public List<String> getTutorsList() {
        return tutorsList;
    }

    public void setTutorsList(List<String> tutorsList) {
        this.tutorsList = tutorsList;
    }

    public int getChallengeVictories() {
        return challengeVictories;
    }

    public void setChallengeVictories(int challengeVictories) {
        this.challengeVictories = challengeVictories;
    }

    public int getChallengeDefeats() {
        return challengeDefeats;
    }

    public void setChallengeDefeats(int challengeDefeats) {
        this.challengeDefeats = challengeDefeats;
    }

    @Override
    public String toString() {
        return "Learner{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", experience=" + experience +
                ", level=" + level +
                ", moneySpent=" + moneySpent +
                ", status=" + status +
                ", tutorsList=" + tutorsList +
                ", challengeVictories=" + challengeVictories +
                ", challengeDefeats=" + challengeDefeats +
                '}';
    }
}
