package ru.itpark.unilearn.serialization;

import ru.itpark.unilearn.entity.Language;
import ru.itpark.unilearn.entity.Tutorial;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class TutorialSerialization {
    public static void main(String[] args) {
        List<Tutorial> list = new ArrayList<>();
        list.add(new Tutorial("Elementary_course", Language.ENGLISH, 8000, 50));
        list.add(new Tutorial("Pre-Intermediate_course", Language.ENGLISH, 12000, 120));
        list.add(new Tutorial("Intermediate_course", Language.ENGLISH, 20000, 140));
        list.add(new Tutorial("Upper-Intermediate_course", Language.ENGLISH, 29000, 170));
        list.add(new Tutorial("Advanced_course", Language.ENGLISH, 40000, 250));
        list.add(new Tutorial("Junior_course", Language.FRENCH, 7000, 45));
        list.add(new Tutorial("Middle_course", Language.FRENCH, 14000, 105));
        list.add(new Tutorial("Master_course", Language.FRENCH, 25000, 140));
        list.add(new Tutorial("Intro_course", Language.ARAB, 6000, 60));
        list.add(new Tutorial("Medium_course", Language.ARAB, 10000, 85));
        list.add(new Tutorial("Character_course", Language.CHINESE, 5000, 40));
        list.add(new Tutorial("Grammar_course", Language.CHINESE, 10000, 70));
        list.add(new Tutorial("Spoken_course", Language.CHINESE, 16000, 140));
        list.add(new Tutorial("First_course", Language.GERMAN, 14000, 85));
        list.add(new Tutorial("Second_course", Language.GERMAN, 17000, 165));
        list.add(new Tutorial("Primary_course", Language.JAPANESE, 11000, 65));
        list.add(new Tutorial("Middle_course", Language.JAPANESE, 19000, 130));
        list.add(new Tutorial("Junior_course", Language.PORTUGUESE, 9000, 50));
        list.add(new Tutorial("Medium_course", Language.PORTUGUESE, 14000, 140));
        list.add(new Tutorial("Intro_course", Language.RUSSIAN, 7000, 65));
        list.add(new Tutorial("Medium_course", Language.RUSSIAN, 12000, 110));
        list.add(new Tutorial("Grammar_course", Language.RUSSIAN, 14000, 140));
        list.add(new Tutorial("Advanced_course", Language.RUSSIAN, 27000, 210));
        list.add(new Tutorial("First_course", Language.SPANISH, 7000, 45));
        list.add(new Tutorial("Second_course", Language.SPANISH, 12000, 95));
        list.add(new Tutorial("Third_course", Language.SPANISH, 21000, 145));
        list.add(new Tutorial("Beginner_course", Language.TURKISH, 8000, 60));
        list.add(new Tutorial("Middle_course", Language.TURKISH, 15000, 130));
        list.add(new Tutorial("Hard_course", Language.TURKISH, 22000, 185));

        try (ObjectOutputStream writer =
                     new ObjectOutputStream(
                             new FileOutputStream("C:\\Users\\Danilka\\Desktop\\FinalProject\\Cast02\\UniLearn\\src\\main\\resources\\static\\Tutorials")
                     )) {
            writer.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
