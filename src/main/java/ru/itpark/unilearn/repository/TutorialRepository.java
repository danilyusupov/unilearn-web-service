package ru.itpark.unilearn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.unilearn.entity.Tutorial;

import java.util.List;

@Repository
public interface TutorialRepository extends CrudRepository<Tutorial, Integer> {
    List<Tutorial> findAll();
    Tutorial findById(int id);
}
