package ru.itpark.unilearn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.unilearn.action.FastTask;

import java.util.List;

@Repository
public interface FastTaskRepository extends CrudRepository<FastTask, Integer> {
    List<FastTask> findAllByLevel(int level);
}
