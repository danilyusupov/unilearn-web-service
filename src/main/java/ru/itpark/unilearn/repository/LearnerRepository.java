package ru.itpark.unilearn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.unilearn.entity.Status;
import ru.itpark.unilearn.entity.Learner;

import java.util.List;

@Repository
public interface LearnerRepository extends CrudRepository<Learner, Integer> {
    List<Learner> findAll();
    List<Learner> findAllByFirstName(String firstName);
    List<Learner> findAllByStatus(Status status);
    Learner findById(int id);
    Learner save(Learner learner);
    Learner findByFirstNameAndSecondName(String firstName, String secondName);
}
