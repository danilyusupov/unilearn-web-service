package ru.itpark.unilearn.action;

import ru.itpark.unilearn.entity.Language;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class FastTask {
    @Id
    @GeneratedValue
    private int id;
    private Language language;
    private int level;
    private String conditionOfTask;
    private String rightAnswer;

    public FastTask() {
    }

    public FastTask(Language language, int level, String conditionOfTask, String rightAnswer) {
        this.language = language;
        this.level = level;
        this.conditionOfTask = conditionOfTask;
        this.rightAnswer = rightAnswer;
    }

    public String getConditionOfTask() {
        return conditionOfTask;
    }

    public void setConditionOfTask(String conditionOfTask) {
        this.conditionOfTask = conditionOfTask;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
