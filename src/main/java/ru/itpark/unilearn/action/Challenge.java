package ru.itpark.unilearn.action;

import org.springframework.stereotype.Component;
import ru.itpark.unilearn.entity.Language;
import ru.itpark.unilearn.entity.Learner;

import java.util.List;

@Component
public class Challenge {
    private Language language;
    private Learner firstLearner;
    private Learner secondLearner;
    private int firstLearnerPoints;
    private int secondLearnerPoints;
    private List<FastTask> tasks;

    public Challenge() {
    }

    public Challenge(Language language, Learner firstLearner, Learner secondLearner, List<FastTask> tasks) {
        this.language = language;
        this.firstLearner = firstLearner;
        this.secondLearner = secondLearner;
        this.tasks = tasks;
    }

    //returns id of the winner
    public int getWinner(){
        if (firstLearnerPoints > secondLearnerPoints){
            return firstLearner.getId();
        } else if (firstLearnerPoints == secondLearnerPoints){
            return -1;
        }
        return secondLearner.getId();
    }

    //returns loser's id
    public int getLoser(){
        if (firstLearnerPoints < secondLearnerPoints){
            return firstLearner.getId();
        } else if (firstLearnerPoints == secondLearnerPoints){
            return -1;
        }
        return secondLearner.getId();
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Learner getFirstLearner() {
        return firstLearner;
    }

    public void setFirstLearner(Learner firstLearner) {
        this.firstLearner = firstLearner;
    }

    public Learner getSecondLearner() {
        return secondLearner;
    }

    public void setSecondLearner(Learner secondLearner) {
        this.secondLearner = secondLearner;
    }

    public int getFirstLearnerPoints() {
        return firstLearnerPoints;
    }

    public void setFirstLearnerPoints(int firstLearnerPoints) {
        this.firstLearnerPoints = firstLearnerPoints;
    }

    public int getSecondLearnerPoints() {
        return secondLearnerPoints;
    }

    public void setSecondLearnerPoints(int secondLearnerPoints) {
        this.secondLearnerPoints = secondLearnerPoints;
    }

    public List<FastTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<FastTask> tasks) {
        this.tasks = tasks;
    }

}
