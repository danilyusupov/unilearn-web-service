package ru.itpark.unilearn.service;

import ru.itpark.unilearn.action.FastTask;
import ru.itpark.unilearn.entity.Language;
import ru.itpark.unilearn.entity.Learner;
import ru.itpark.unilearn.entity.Status;
import ru.itpark.unilearn.entity.Tutorial;

import java.util.List;

public interface LearnerService {
    List<Learner> findAll();
    void add(Learner learner);
    void remove(int id);
    List<Learner> findByFirstName(String firstName);
    Learner findLearner(String firstName, String secondName);
    List<Learner> findByStatus(Status status);
    void addPurchase(int id, int tutorId);
    Learner getById(int id);
    void update(Learner learner);
    List<String> getTutorsList(int learnerId);
    String makeChallenge(int initLearnerId, int targetLearnerId, Language language);
    List<FastTask> getTasks(Learner first, Learner second);
}
