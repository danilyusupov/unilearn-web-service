package ru.itpark.unilearn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.unilearn.entity.Tutorial;
import ru.itpark.unilearn.repository.TutorialRepository;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

@Service
public class TutorialServiceImpl implements TutorialService {
    private final TutorialRepository repository;

    @Autowired
    public TutorialServiceImpl(TutorialRepository repository) {
        this.repository = repository;
        //Initialization of default tutorials with serialization
//        List<Tutorial> tutorialList = null;
//        try(ObjectInputStream in =
//                    new ObjectInputStream(
//                            new FileInputStream("C:\\Users\\Danilka\\Desktop\\FinalProject\\Cast02\\UniLearn\\src\\main\\resources\\static\\Tutorials")
//                    )) {
//            tutorialList = (List<Tutorial>) in.readObject();
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        repository.save(tutorialList);
    }

    @Override
    public List<Tutorial> findAll() {
        return repository.findAll();
    }

    @Override
    public void add(Tutorial tutorial) {
        repository.save(tutorial);
    }

    @Override
    public void remove(int id) {
        repository.delete(repository.findById(id));
    }

    @Override
    public Tutorial getById(int id) {
        return repository.findById(id);
    }
}