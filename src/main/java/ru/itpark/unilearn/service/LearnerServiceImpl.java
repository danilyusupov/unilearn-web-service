package ru.itpark.unilearn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.unilearn.action.Challenge;
import ru.itpark.unilearn.action.FastTask;
import ru.itpark.unilearn.entity.Language;
import ru.itpark.unilearn.entity.Learner;
import ru.itpark.unilearn.entity.Status;
import ru.itpark.unilearn.entity.Tutorial;
import ru.itpark.unilearn.repository.FastTaskRepository;
import ru.itpark.unilearn.repository.LearnerRepository;

import java.util.List;

@Service
public class LearnerServiceImpl implements LearnerService {
    private final LearnerRepository repository;
    private final TutorialService tutorialService;
    private final FastTaskRepository taskRepository;

    @Autowired
    public LearnerServiceImpl(LearnerRepository repository, TutorialService tutorialService, FastTaskRepository taskRepository) {
        this.repository = repository;
        this.tutorialService = tutorialService;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Learner> findAll() {
        return repository.findAll();
    }

    @Override
    public void add(Learner learner) {
        repository.save(learner);
    }

    @Override
    public void remove(int id) {
        repository.delete(id);
    }

    @Override
    public List<Learner> findByFirstName(String firstName) {
        return repository.findAllByFirstName(firstName);
    }

    @Override
    public Learner findLearner(String firstName, String secondName) {
        return repository.findByFirstNameAndSecondName(firstName, secondName);
    }

    @Override
    public Learner getById(int id) {
        return repository.findById(id);
    }

    @Override
    public void update(Learner learner) {

        //initializing status
        if (learner.getMoneySpent() > 5000) {
            learner.setStatus(Status.BRONZE);
        } else if (learner.getMoneySpent() > 10000) {
            learner.setStatus(Status.SILVER);
        } else if (learner.getMoneySpent() > 20000) {
            learner.setStatus(Status.GOlD);
        }

        //initializing level
        int experience = learner.getExperience();
        int level;
        if (experience > 0 && experience < 100) {
            level = 0;
        } else if (experience < 200) {
            level = 1;
        } else if (experience < 300) {
            level = 2;
        } else if (experience < 400) {
            level = 3;
        } else if (experience < 500) {
            level = 4;
        } else if (experience < 600) {
            level = 5;
        } else if (experience < 700) {
            level = 6;
        } else if (experience < 800) {
            level = 7;
        } else if (experience < 900) {
            level = 8;
        } else if (experience < 1000) {
            level = 9;
        } else {
            level = 10;
        }
        learner.setLevel(level);


        repository.save(learner);
    }

    @Override
    public List<Learner> findByStatus(Status status) {
        return repository.findAllByStatus(status);
    }

    @Override
    public void addPurchase(int learnerId, int tutorId) {
        Learner learner = repository.findById(learnerId);
        Status temp = learner.getStatus();
        Tutorial tutorial = tutorialService.getById(tutorId);
        int price = tutorial.getPrise();
        switch (temp) {
            case GOlD:
                price *= 0.8;
                break;
            case SILVER:
                price *= 0.9;
                break;
            case BRONZE:
                price *= 0.95;
                break;

        }
        learner.setMoneySpent(learner.getMoneySpent() + price);
        learner.setExperience(learner.getExperience() + tutorial.getExperience());
        learner.getTutorsList().add(tutorial.getName());
        update(learner);
        repository.save(learner);
    }

    @Override
    public List<String> getTutorsList(int learnerId) {
        return repository.findById(learnerId).getTutorsList();
    }


    @Override
    public String makeChallenge(int intiLearnerId, int targetLearnerId, Language language) {
        if (intiLearnerId != targetLearnerId) {
            Learner init = repository.findById(intiLearnerId);
            Learner target = repository.findById(targetLearnerId);
            Challenge challenge = new Challenge(language, init, target, getTasks(getById(intiLearnerId), getById(targetLearnerId)));
            challenge.setFirstLearnerPoints((int) (Math.random() * 10));
            challenge.setSecondLearnerPoints((int) (Math.random() * 10));
            if (challenge.getWinner() != -1 && challenge.getLoser() != -1) {
                //setting experience by the end of challenge
                Learner winner = repository.findById(challenge.getWinner());
                Learner loser = repository.findById(challenge.getLoser());
                winner.setExperience(winner.getExperience() + 5);
                loser.setExperience(loser.getExperience() - 2);
                winner.setChallengeVictories(winner.getChallengeVictories() + 1);
                loser.setChallengeDefeats(loser.getChallengeDefeats() + 1);
                update(winner);
                update(loser);
                if (repository.findById(intiLearnerId).equals(repository.findById(challenge.getWinner()))) {
                    return "You win!";
                }
                return "You lose...";
            }
        }
        return "Draw.";
    }

    @Override
    public List<FastTask> getTasks(Learner first, Learner second) {
        int level = (first.getLevel() + second.getLevel()) / 2;
        return taskRepository.findAllByLevel(level);
    }

}