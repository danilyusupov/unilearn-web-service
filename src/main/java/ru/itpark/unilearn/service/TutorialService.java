package ru.itpark.unilearn.service;

import ru.itpark.unilearn.entity.Tutorial;

import java.util.List;

public interface TutorialService {
    List<Tutorial> findAll();
    void add(Tutorial tutorial);
    void remove(int id);
    Tutorial getById(int id);
}