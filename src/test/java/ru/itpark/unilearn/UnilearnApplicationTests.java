package ru.itpark.unilearn;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.itpark.unilearn.entity.Learner;
import ru.itpark.unilearn.service.LearnerService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UnilearnApplicationTests {
	@Autowired
	LearnerService learnerService;

	@Test
	public void testAddLearners() {
		int initCount = learnerService.findAll().size();
		learnerService.add(new Learner("Vasya", "Smirnov"));
		learnerService.add(new Learner("Ivan", "Ivanovich"));
		learnerService.add(new Learner("Fedor", "Olegov"));
		int postCount = learnerService.findAll().size();
		assertEquals(3, postCount - initCount);
	}

}
